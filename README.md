# CMapReanalysed2016

The CMap raw data (.CEL) files were all reanalysed with a classical R based pipeline, followed by batch correction.
That data was used by our group for a series of projects.

To complete the provenance, the script that implemented the reanalysis at the
time is attached below. For new projects, and for newer data, please refer
to the repositories 
[MolDrugEffectsDB](https://bitbucket.org/ibima/moldrugeffectsdb/src/master/)
and
[MolDrugEffectsDBData](https://bitbucket.org/ibima/moldrugeffectsdbdata/src/master/).

This repository offers the RData files for the
[HT-HGU133A](http://www.affymetrix.com/support/technical/byproduct.affx?product=ht_hg-u133_set)
platform of affymetrix.

| File                                    | Description             |
|-----------------------------------------|-------------------------|
| CMap_HTHGU133A_FC_19_1_16.RData         | log fold changes        |
| CMap_HTHGU133A_Magnitudes_16_3_16.RData | log absolute expression |



```R
# Created by Mathias Ernst, 2016

#library(cMAP)
library(affy)
library(sva)
library(preprocessCore)
library(hthgu133a.db)
library(hgu133a.db)
library(affyPLM)
#library(hthgu133acdf)

#https://portals.broadinstitute.org/cmap/cmap_instances_02.xls
cmap.anno = read.table("cmap_instances_02.csv",header=T,sep=";",stringsAsFactors = F)
cmap.anno$perturbation_scan_id = gsub( "'" , "" , cmap.anno$perturbation_scan_id )
cmap.anno$vehicle_scan_id = gsub("'","",cmap.anno$vehicle_scan_id )
cmap.anno$vehicle_scan_id = gsub("^\\.","",cmap.anno$vehicle_scan_id)
cmap.anno$scan_id = NA
cmap.anno$CaseControl = NA

cmap.anno$concentration = as.character( cmap.anno$concentration )
cmap.anno$concentration = gsub("^(\\d)e","\\1,00E",cmap.anno$concentration)
cmap.anno$concentration = gsub("\\.",",",cmap.anno$concentration)
cmap.anno$concentration = gsub("e","E",cmap.anno$concentration)

i = 390

L = lapply( 1:nrow(cmap.anno) , function (i) {
  x = cmap.anno[i,]

  ctrl.set = unlist( strsplit( as.character(x[10]) , "\\." ) )

  if ( length( ctrl.set ) > 2 ) {
      ctrl.lines = lapply( ctrl.set , function (y) {
        x2 = x
        x2$scan_id = gsub( "\\..+$" , paste(".",y,sep="") , x$perturbation_scan_id )
        x2
        })
      ctrl.lines = do.call( rbind , ctrl.lines )
  } else {
    ctrl.lines = x
    ctrl.lines$scan_id = x[10]
  }
  ctrl.lines$CaseControl = "control"
  case.line = x
  case.line$scan_id = case.line$perturbation_scan_id
  case.line$CaseControl = "case"

  rbind( case.line , ctrl.lines )
})

cmap.anno.expanded = do.call( rbind , L )
cmap.anno.expanded$scan_id = unlist( cmap.anno.expanded$scan_id )
cmap.anno.expanded$celfile.name = paste( cmap.anno.expanded$scan_id , ".CEL" , sep="" )


cmap.anno.expanded$SM.conc.cell = paste(cmap.anno.expanded$cmap_name,cmap.anno.expanded$concentration,cmap.anno.expanded$cell,sep="_")

cmap.anno.expanded.red = cmap.anno.expanded[ ! duplicated( cmap.anno.expanded$scan_id ) , ]


rownames(cmap.anno.expanded.red) = cmap.anno.expanded.red$celfile.name

table( cmap.anno.expanded.red$array)

batch.tab = as.data.frame( table( cmap.anno.expanded.red$batch_id ) )
table(batch.tab$Freq)

cmap.anno.expanded.red = merge( cmap.anno.expanded.red , batch.tab , by.x="batch_id",by.y="Var1",all.x=T )
cmap.anno.expanded.red = cmap.anno.expanded.red[ order( cmap.anno.expanded.red$Freq , decreasing = T ) , ]

cmap.anno.expanded.red = cmap.anno.expanded.red[ cmap.anno.expanded.red$Freq >= 10 , ]
rownames(cmap.anno.expanded.red) = cmap.anno.expanded.red$celfile.name

table( cmap.anno.expanded.red$cell , cmap.anno.expanded.red$batch_id)

all.cell.files = list.files( path = "." , pattern = "CEL" , full.names = T , recursive = T , include.dirs = T )
z = sapply( strsplit( all.cell.files , split = "\\/" ) , "[" , 4 )

names(all.cell.files) = z


list.of.rma.res = list()

for ( my.batch.id in unique( cmap.anno.expanded.red$batch_id ) ) {
  #my.batch.id = unique( cmap.anno.expanded.red$batch_id )[1]
  print(my.batch.id)

  batch.df = cmap.anno.expanded.red[ cmap.anno.expanded.red$batch_id == my.batch.id , ]
  batch.samples = batch.df$scan_id
  batch.samples = paste( batch.samples , ".CEL" , sep="")

  batch.cell.files = all.cell.files[ batch.samples ]

  my.batch = ReadAffy( filenames = batch.cell.files )
  my.plm = fitPLM(my.batch)

  nuse.res = NUSE(my.plm,type="stats")
  y  = nuse.res[ "median" , ] + (nuse.res[ "IQR" , ] / 2 )  <= 1.05

  my.batch = my.batch[ , y ]
  batch.df = droplevels.data.frame( batch.df[ y , ] )
  my.rma = rma(my.batch)

  xx = exprs(my.rma)

  my.res = list( "xx" = xx , "pData" = batch.df )

  my.name = paste( my.batch@cdfName , unique(batch.df$cell) , my.batch.id , sep=":" )
  list.of.rma.res[[ my.name ]] = my.res

}

#save( list.of.rma.res , file = "CMAP_postRMA_intermed.RData" )

a = strsplit( x = names( list.of.rma.res ) , split = ":" )
a.chip = sapply( a , "[" , 1)
a.cell = sapply( a , "[" , 2)

chip.cell.fac = factor(paste( a.chip , a.cell , sep="_"))

ll.noBE = lapply( levels(chip.cell.fac) , function (y) {
  ll = list.of.rma.res[ chip.cell.fac == y ]
  ll.xx = lapply( ll , "[[" , "xx" )
  ll.pheno = lapply( ll , "[[" , "pData" )
  xx = do.call( cbind , ll.xx )
  xx.pheno = do.call( rbind , ll.pheno )

  my.design = model.matrix( ~ xx.pheno$CaseControl )
  batch.fac = factor( xx.pheno$batch_id )

  xx.noBE = ComBat( xx , batch = batch.fac , mod = my.design )

  xx.noBE
})
names(ll.noBE) = levels(chip.cell.fac)

names(ll.noBE)

X = do.call( cbind , ll.noBE[3:5] )
#X = do.call( cbind , ll.noBE[1:2] )

pheno.df = cmap.anno.expanded.red[ colnames(X) , ]


X.norm = normalize.quantiles( X )
rownames(X.norm) = rownames(X)
colnames(X.norm) = colnames(X)

x.entrez = mget( rownames(X) , hthgu133aENTREZID  )
#x.entrez = mget( rownames(X) , hgu133aENTREZID )

table( sapply( x.entrez , length ) )

rownames(X.norm) = unlist( x.entrez)
X.norm = X.norm[ !is.na(rownames(X.norm)),]

table( table( rownames(X.norm)))

#Z = aggregate( X.norm , by=list(rownames(X.norm)),median)
#cmap.matrix = as.matrix( Z[ , 2:ncol(Z)])
#rownames(cmap.matrix) = Z$Group.1

list.of.agg = list()

for ( i in 1:ncol(X.norm) ) {
  if ( i %% 50 == 0 ) { print(i) }
  x.agg = tapply( X.norm[ , i ] , factor( rownames(X.norm)) , median )
  list.of.agg[[ i ]] = x.agg
}

cmap.matrix = do.call( cbind , list.of.agg )
colnames(cmap.matrix) = colnames(X)


my.pca = prcomp( t(cmap.matrix) )
plot(my.pca$x[,1:2])

#save( cmap.matrix , file = "CMAP_postAggregate.RData" )

## log FC calculations

pheno.df = cmap.anno.expanded[ cmap.anno.expanded$celfile.name %in% colnames(cmap.matrix) , ]

case.control.tab = tapply( pheno.df$CaseControl , factor( pheno.df$SM.conc.cell ) , unique )
case.control.tab = data.frame( sapply( case.control.tab , length ) )

pheno.df.x = merge( pheno.df , case.control.tab , by.x = "SM.conc.cell" , by.y = "row.names" , all.x = T )

pheno.df = pheno.df.x[ pheno.df.x$sapply.case.control.tab..length. == 2 , ]

all.treatments = unique( pheno.df$SM.conc.cell )

i = 50

list.of.FC = lapply( 1:length(all.treatments) , function (i) {
  if ( i %% 50 == 0 ) { print(i) }
  my.drug = all.treatments[i]
  pheno.set = pheno.df[ pheno.df$SM.conc.cell == my.drug , ]
  pheno.set = pheno.set[ ! duplicated( pheno.set$scan_id ) , ]
  xx = cmap.matrix[ , pheno.set$celfile.name ]
  CaseControl.fac = factor( pheno.set$CaseControl )

  xx.means = t( apply( xx , 1 , tapply , CaseControl.fac , mean ) )
  xx.fc = xx.means[,"case"] - xx.means[,"control"]
  xx.fc
})

cmap.fc = do.call( cbind , list.of.FC )
colnames(cmap.fc) = all.treatments

save(cmap.fc , file = "CMap_FC_HTHGU133a.RData")
#save(cmap.fc , file = "CMap_FC_HGU133a.RData")

my.pca = prcomp( t( cmap.fc ) )

z = strsplit( x = colnames(cmap.fc) , split = "\\." )
cmap.cell = factor(sapply( z , function (x) { x[length(x)] } ) )


my.col = rep("grey",ncol(cmap.fc))
my.col[ grep( "tricho|SAHA|vorino", colnames(cmap.fc)) ] = "green"
my.col[ grep( "salicyl", colnames(cmap.fc)) ] = "green"

plot(my.pca$x[,1:2],cex=0.5 , col = my.col )

plot(my.pca$x[,1:2],cex=0.1 )
text(my.pca$x[,1:2],labels=colnames(cmap.fc),cex=0.2)

View(cmap.fc[,1:10])

plot(my.pca$x[,1:2] , col = c("red","green","blue")[as.numeric(cmap.cell)])


#########

y = grep("etopos",colnames(cmap.fc))
Z = cmap.fc[,y]

i = 50
Z = cmap.fc[,i]


Z = as.data.frame(Z)
Z$symbol = unlist( mget(rownames(Z),org.Hs.egSYMBOL))

###

i = 50
my.drug = all.treatments[i]
pheno.set = pheno.df[ pheno.df$SM.conc.cell == my.drug , ]
xx = cmap.matrix[ , pheno.set$celfile.name ]
CaseControl.fac = factor( pheno.set$CaseControl )

my.pca = prcomp( t(xx) )

l = factor( pheno.set$CaseControl )

plot(my.pca$x[,1:2] , xlim=c(-10,30) , col=c("green","red")[as.numeric(l)]  )
text(my.pca$x[,1:2] , labels = pheno.set$scan_id , cex = 0.4)

pairs(my.pca$x[,1:4] , col=c("green","red")[as.numeric(l)] , pch=19)

#######
check against old cmap


z = grep( "acetylsalicy" , colnames(cmap.fc.old) )
A2 = cmap.fc.old[,z]

a = sort( A2[,2] , decreasing = T )
a = unlist( mget( names(a)[1:100] , org.Hs.egSYMBOL , ifnotfound = NA) )

b = sort( A2[,2] , decreasing = F )
b = unlist( mget( names(b)[1:100] , org.Hs.egSYMBOL , ifnotfound = NA ) )

View( cbind(a,b))

text(my.pca$x[,1:2],labels=colnames(cmap.fc),cex=0.5)


plot( my.pca$x[,1:2] , col = rainbow(length(levels(l)))[as.numeric(l)] , pch=19 )


system.time( aggregate( X.norm[,1:500] , by=list(rownames(X.norm)),median) )



y = factor( rownames(X.norm) )

system.time(
  Z = sapply( 1:ncol(X.norm) , function (i) {
    if ( i %% 100 == 0 ) { print(i) }
    tapply( X.norm[,i] , y , median )
  })
)


my.pca = prcomp( t( X.norm ) )

l = factor( pheno.df$CaseControl )
plot(my.pca$x[,1:2] , col = rainbow(length(levels(l)))[as.numeric(l)] , cex=0.3 )
text(my.pca$x[,1:2] , labels = xx.pheno$cmap_name , cex = 0.7 )


pairs(my.pca$x[,1:4] , col = rainbow(length(levels(l)))[as.numeric(l)]  , pch = 19 )

my.resid = apply( xx , 1 , function (y) {
  resid( lm( y ~ l ) )
})
my.resid = t(my.resid)

my.pca = prcomp( t( my.resid ) )


a = tapply( cmap.anno.expanded$batch_id , factor( cmap.anno.expanded$scan_id ) , unique )


table( table( cmap.anno.expanded$batch_id) )


my.batch = ReadAffy(celfile.path = "CEL/cmap_build02.volume1of7/",compress = "bz2" )




L = lapply( 1:nrow(cmap.anno) , function (i) {
  x = cmap.anno[i,]

  ctrl.set = unlist( strsplit( as.character(x[10]) , "\\." ) )
  length(ctrl.set)
})


a = unlist(L)

```
